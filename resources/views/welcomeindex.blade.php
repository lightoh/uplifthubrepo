<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>        
	    <!-- Meta Descriptions -->
	    <meta charset="utf-8"/>
	    <title>Uplift Academy :: Home</title>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta content="width=device-width, initial-scale=1" name="viewport"/>
	    <meta content="Uplift Academy." name="Learn how to code from scratch at your own pace"/>
	    <meta content="Uplift" name="Uplift Nigeria"/><meta content="Coding Academy" name="Uplift Nigeria"/>
	    
	    <!-- css Files -->
	    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" type="text/css" href="css/uplift.css">
	    <link rel="stylesheet" type="text/css" href="css/font-awesome-4.7.0/css/font-awesome.min.css">

	    <!-- Js Script -->
	    <script type="text/javascript" href=""></script>
	</head>
	<body>
		<div class="container">
			<div class="image-carousel">
				<div class="css-carousel">

				</div>
			</div>
			<div class="content">
				<div class="heading">
					<h1>Uplift Academy</h1>
					<p>Coding classes for beginners: Learn to code responsive websites, build interactive apps, and become a job-ready Computer Expert at Uplift Academy.</p>
				</div>
				<div class="heading"><h2>Avaliable Courses!</h2></div>
					<div class="courses">
						<div class="course-list">
							<div class="list">
								<i class="fa fa-desktop fa-3x"></i>
								<h1>Computer Appreciation</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-desktop fa-3x"></i>
								<h1>Computer Appreciation Advanced</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript...</p>
								<a class="apply" style="margin-top: 0px;" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-desktop fa-3x"></i>
								<h1>Computer Appreciation Pro</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-key fa-3x"></i>
								<h1>Starter Program</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-laptop fa-3x"></i>
								<h1>Front End Development</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-gear fa-3x"></i>
								<h1>Back End Development</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-wordpress fa-3x"></i>
								<h1>CMS :: Wordpress</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
							<div class="list">
								<i class="fa fa-dashboard fa-3x"></i>
								<h1>IoT & Programming</h1>
								<p>Here you'll learn how to use HTML and CSS and JavaScript to make responsive webpages...</p>
								<a class="apply" href="javascript:;">Apply Now</a>
							</div>
						</div>
					</div>
			</div>
		</div>
	</body>
</html>