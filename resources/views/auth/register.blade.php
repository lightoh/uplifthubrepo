@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">{{ (\Session::has('mPlan') && !empty($plan))? 'Registration Continues - Fill In Details And Proceed' : 'Register' }}</div>
                <div class="panel-body">
                    @if(\Session::has('mPlan') && !empty($plan))
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Your Membership Plan</label>

                            <div class="col-md-6">
                                <input id="plan" type="hidden" class="form-control" name="plan" value="{{ $plan}}" required>
                                <div id="plan" type="disabled" class="alert-success alert"  >{{ $current_member_plan->membership_title}}</div>

                                @if ($errors->has('plan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="planfee" class="col-md-4 control-label">Amount To Pay</label>

                            <div class="col-md-6">
                                <input id="planfee" type="hidden" class="form-control" name="plan" value="{{ $plan}}" required>
                                <div type="disabled" class="alert-success alert"  >{{ $current_member_plan->membership_fee}}</div>

                                @if ($errors->has('plan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">FirstName</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">LastName</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('othername') ? ' has-error' : '' }}">
                            <label for="othername" class="col-md-4 control-label">OtherName</label>

                            <div class="col-md-6">
                                <input id="othername" type="text" class="form-control" name="othername" value="{{ old('othername') }}" required autofocus>

                                @if ($errors->has('othername'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('othername') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phoneno') ? ' has-error' : '' }}">
                            <label for="phoneno" class="col-md-4 control-label">PhoneNo:</label>

                            <div class="col-md-6">
                                <input id="phoneno" type="text" class="form-control" name="phoneno" value="{{ old('phoneno') }}" required autofocus>

                                @if ($errors->has('phoneno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phoneno') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">

                            <label for="gender" class="col-md-4 control-label">Gender:</label>
                            <div class="col-md-6">
                                <select id="gender" class="form-control" name="gender" required autofocus>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                </select>

                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm"  type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Proceed To Payment To Complete Registration
                                </button>
                            </div>
                        </div>
                    </form>
                        {{-- Clear The Session Value --}}
                        @php( \Session::forget('mPlan') )
                  @else
                        <div class="panel panel-heading bg-info panel-info text-center"><strong class="text-uppercase">Select Membership Plan Please</strong></div>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        @php
                            $formatter = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY);
                            $count = 0;
                            $record = count($memberships);

                            $numOfSlides = 0;
                            //calculate the number of slides if a slide contains 2 memberships
                            for($kount = 0; $kount<$record; $kount++){
                                if($kount%2 ==0){ $numOfSlides++; }
                            }

                        @endphp

                        <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @for($kount = 0; $kount<$record; $kount++)
                                    @if($kount%2 ==0)
                                        <li data-target="#myCarousel" data-slide-to="{{$numOfSlides++}}" {{ $kount == 0? 'class="active"': '' }}></li>
                                    @endif
                                @endfor
                            </ol>

                            <!-- Wrapper for Membership Payment Selection slides -->
                            <div class="carousel-inner">
                                {{--Loop Through to Get the memberships --}}
                                @for($loop_counter = 0; $loop_counter < $record; $loop_counter++)
                                    @php($eachMembership = $memberships[$loop_counter])
                                    @if(($loop_counter % 2) == 0)
                                <div class="item {{$loop_counter == 0? 'active': ''}}">
                                    @endif
                                    <div class="col-md-6 border-line">
                                        <div class="panel {{ $eachMembership->status == 1 ? 'bg-success panel-success' : 'bg-warning panel-warning' }}">
                                            <div class="panel panel-heading  text-center align-content-center">
                                                <h4><strong>{{$eachMembership->membership_title}}</strong></h4>
                                                <hr>
                                                <img src="{{Storage::exists($eachMembership->image)? \Storage::url($eachMembership->image) : \Storage::url('images/defaultmembershiplogo.jpg') }}" class="img-fluid img-responsive" id="image_logo_carousel"  alt="{{ $eachMembership->membership_title }}" />
                                            </div>
                                            <div class="panel-body">
                                                <p>{{ substr(str_pad($eachMembership->description,50 ,"  " ), 0, 50) }}</p>
                                                <h4 class="text-info">Fee: {!! $formatter->format($eachMembership->membership_fee, 0 ) !!}</h4>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="{{url('register/'.$eachMembership->id)}}" class="btn btn-success btn-lg btn-block" >Select</a>
                                            </div>
                                        </div>
                                    </div>
                                    @if(($loop_counter % 2) == 0)
                                </div>
                                    @endif


                                @endfor

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
