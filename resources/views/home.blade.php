@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="row">
                    <div class="list-group">
                        @php
                            $menu = isset($menu)? $menu : "dashboard";
                        @endphp
                        {{-- Menu Listing Here --}}
                        @if(\Auth::user()->level == 1)
                            <div class="list-group-item list-group-item-heading">
                                Menu
                            </div>
                            <a href="{{route('home')}}"
                               class="list-group-item {{ $menu == 'dashboard' ? 'active': ''}} ">
                                Dashboard <i class="glyphicon glyphicon-dashboard"></i>
                            </a>
                            <a href="{{route('membership.create')}}"
                               class="list-group-item {{ $menu == 'membership.create' ? 'active': ''}}">
                                Create Membership <i class="glyphicon-user glyphicon"></i>
                            </a>
                            <a href="{{route('membership.index')}}"
                               class="list-group-item {{ $menu == 'membership.index' ? 'active': ''}}">
                                Manage Membership <i class="glyphicon-user glyphicon"></i>
                            </a>
                            <a href="{{route('members.index')}}"
                               class="list-group-item {{ $menu == 'members.index' ? 'active': ''}}">
                                Manage Members <i class="glyphicon-user glyphicon"></i>
                            </a>
                        @else
                            <div class="list-group-item list-group-item-heading">
                                Menu
                            </div>
                            <a href="{{route('home')}}"
                               class="list-group-item {{ $menu == 'dashboard' ? 'active': ''}} ">
                                Dashboard <i class="glyphicon glyphicon-dashboard"></i>
                            </a>
                        {{-- TODO Implement the Payment History Application --}}
                            <a href="{{route('home')}}"
                               class="list-group-item {{ $menu == 'paymenthistory' ? 'active': ''}} ">
                                Payment History <i class="glyphicon glyphicon-credit-card"></i>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-0">
                @section('subcontent')
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>

                        <div class="panel-body">
                            You are logged in!
                        </div>
                    </div>
                @show
            </div>
        </div>
    </div>
@endsection
