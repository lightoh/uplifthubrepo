@extends('membership.sublayout')

@php($title = isset($title)? $title : 'Create New Membership')

@php($menu = isset($menu)? $menu : 'membership.create')


@section('subcontent')
   <div class="panel panel-default">
                <div class="panel-heading">Create New Membership</div>

                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('membership.store') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <fieldset class="col-md-6 col-md-offset-4 ">
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Related Image: </label>
                                <input type="file" class="form-control-file" name="image">
                            </div>
                            <div class="form-group{{ $errors->has('membership_title') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Title: </label>
                                <input  required="required" class="form-control" type="text" name="membership_title" value="{{old('membership_title', '')}}" placeholder="Membership Title">
                            </div>
                            
                            <div class="form-group{{ $errors->has('membership_fee') ? ' has-error' : '' }}" >
                            <label class="form-control-label">Membership Fee (&#8358;): </label>
                                <input  required="required" class="form-control" type="number" name="membership_fee" value="{{old('membership_fee', '')}}" placeholder="Membership Fee">
                            </div>
                            <div class="form-group{{ $errors->has('membership_desc') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Description: </label>
                                <textarea name="membership_desc"  required="required" class="form-control" placeholder="Membership Description">{{old('membership_desc', '')}}</textarea>
                            </div>
                            <div class="form-group">
                                <input  class="btn btn-success" type="submit" name="Submit" value="Add Membership">
                            </div>
                        </fieldset>
                    </form>
                </div>
    </div>
@endsection
