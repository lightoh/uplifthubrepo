@extends('membership.sublayout')

@php($title = isset($title)? $title : "Update  Membership")

@php($menu = isset($menu)? $menu : 'membership.index')


@section('subcontent')
   <div class="panel panel-default">
                <div class="panel-heading">View Membership</div>

                <div class="panel-body">
                    @if (\Session::has('success_message'))
                        <div class="alert alert-success">{{ \Session::get('success_message') }}</div>
                    @endif

                    @if (\Session::has('error_message' ))
                        <div class="alert alert-danger">{{ \Session::get('error_message' )}}</div>
                    @endif
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('membership/'.$membership->id) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
                        <fieldset class="col-md-6 col-md-offset-4 ">

                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <img src="{{Storage::exists($membership->image)? \Storage::url($membership->image) : \Storage::url('images/defaultmembershiplogo.jpg') }}" class="img-fluid img-responsive" id="image_logo"  alt="{{ $membership->membership_title}}">
                                <label class="form-control-label">Membership Related Image: </label>
                            </div>
                            <div class="form-group{{ $errors->has('membership_title') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Title: </label>
                                <div  required="required" class="form-control-static" type="text" name="membership_title">{{ $membership->membership_title }}</div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('membership_fee') ? ' has-error' : '' }}" >
                            <label class="form-control-label">Membership Fee (&#8358;): {{ $membership->membership_fee }}</label>
                            </div>
                            <div class="form-group{{ $errors->has('membership_desc') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Description: </label>
                                <div name="membership_desc"  required="required" class="form-control-static" placeholder="Membership Description">{{$membership->description}}</div>
                            </div>
                            <div class="form-group">
                                <a href="{{url('membership/'.$membership->id.'/edit')}}"  class="btn btn-success" >Edit Membership</a>
                                <a href="{{url('membership/')}}"  class="btn btn-info" >View All Membership</a>
                            </div>
                        </fieldset>
                    </form>
                </div>
    </div>
@endsection
