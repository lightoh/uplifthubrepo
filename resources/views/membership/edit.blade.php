@extends('membership.sublayout')

@php($title = isset($title)? $title : "Update  Membership")

@php($menu = isset($menu)? $menu : 'membership.index')


@section('subcontent')
   <div class="panel panel-default">
                <div class="panel-heading">Update Current Membership</div>

                <div class="panel-body">
                    @if (\Session::has('success_message'))
                        <div class="alert alert-success">{{ \Session::get('success_message') }}</div>
                    @endif

                    @if (\Session::has('error_message' ))
                        <div class="alert alert-danger">{{ \Session::get('error_message' )}}</div>
                    @endif
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('membership/'.$membership->id) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
                        <fieldset class="col-md-6 col-md-offset-4 ">

                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <img src="{{Storage::exists($membership->image)? \Storage::url($membership->image) : \Storage::url('images/defaultmembershiplogo.jpg') }}" class="img-fluid img-responsive" id="image_logo"  alt="{{ $membership->membership_title}}">
                                <label class="form-control-label">Membership Related Image: </label>
                                <input type="file" class="form-control-file" name="image">
                            </div>
                            <div class="form-group{{ $errors->has('membership_title') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Title: </label>
                                <input  required="required" class="form-control" type="text" name="membership_title" value="{{old('membership_title', $membership->membership_title)}}" placeholder="Membership Title">
                            </div>
                            
                            <div class="form-group{{ $errors->has('membership_fee') ? ' has-error' : '' }}" >
                            <label class="form-control-label">Membership Fee (&#8358;): </label>
                                <input  required="required" class="form-control" type="number" name="membership_fee" value="{{old('membership_fee', $membership->membership_fee)}}" placeholder="Membership Fee">
                            </div>
                            <div class="form-group{{ $errors->has('membership_desc') ? ' has-error' : '' }}">
                                <label class="form-control-label">Membership Description: </label>
                                <textarea name="membership_desc"  required="required" class="form-control" placeholder="Membership Description">{{old('membership_desc', $membership->description)}}</textarea>
                            </div>
                            <div class="form-group">
                                <input  class="btn btn-success" type="submit" name="Submit" value="Update Membership">
                            </div>
                        </fieldset>
                    </form>
                </div>
    </div>
@endsection
