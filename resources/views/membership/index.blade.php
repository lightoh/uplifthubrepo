@extends('membership.sublayout')

@php($title = isset($title)? $title : 'All Membership')

@php($menu = isset($menu)? $menu : 'membership.index')


@section('subcontent')
   <div class="panel panel-default">
                <div class="panel-heading">All Membership</div>
                   
                <div class="panel-body">
                    @if (\Session::has('success_message'))
                        <div class="alert alert-success">{{ \Session::get('success_message') }}</div>
                    @endif

                    @if (\Session::has('error_message' ))
                        <div class="alert alert-danger">{{ \Session::get('error_message' )}}</div>
                    @endif

                    <div class="row">
                        @php($formatter = new \NumberFormatter('en_NG', \NumberFormatter::CURRENCY))
                        @php($count = 0)
                        @foreach ($memberships as $key => $eachMembership)
                        
                            <div class="col-md-4 ">
                                <div class="panel {{ $eachMembership->status == 1 ? 'bg-success panel-success' : 'bg-warning panel-warning' }}">
                                    <div class="panel panel-heading  text-center align-content-center">
                                        <h4><strong>{{$eachMembership->membership_title}}</strong></h4>
                                        <hr>
                                        <img src="{{Storage::exists($eachMembership->image)? \Storage::url($eachMembership->image) : \Storage::url('images/defaultmembershiplogo.jpg') }}" class="img-fluid img-responsive" id="image_logo"  alt="{{ $eachMembership->membership_title }}" />
                                    </div>
                                    <div class="panel-body">
                                        <p>{{ substr(str_pad($eachMembership->description,50 ,"  " ), 0, 50) }}</p>
                                        <h4 class="text-info">Fee: {!! $formatter->format($eachMembership->membership_fee, 0 ) !!}</h4>
                                    </div>
                                    <div class="panel-footer">
                                        <a href="#" class="btn btn-success btn-block" >Select</a>
                                        <a href="{{url('membership/'.$eachMembership->id.'/edit')}}" class="btn btn-info btn-block" >Edit</a>

                                        {{-- This is the form --}}

                                        <form class="form-horizontal" method="post" action="{{url('membership/'.$eachMembership->id)}}">

                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="DELETE">         
                                            <div class="">
                                                @if ($eachMembership->status == 1)
                                                    <input type="submit" href="{{url('membership/'.$eachMembership->id.'/edit')}}" class="btn btn-block btn-danger btn-secondary" value="De-Activate" />
                                                @else
                                                    <input type="submit" href="{{url('membership/'.$eachMembership->id.'/edit')}}" class="btn btn-warning btn-block" value="Activate" />
                                                @endif
                                            </div>
                                        </form>
                                        {{-- This is the end of the form for Activate and De-Activate --}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-4"></div>
                    </div>
                </div>
    </div>
@endsection
