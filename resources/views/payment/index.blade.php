@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ 'Registration Continues - Click To Pay' }}</div>
                    <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" >
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Your Membership Plan</label>

                                    <div class="col-md-6">

                                        <div id="plan" type="disabled" class="alert-success alert"  >{{ $membershipDetail->membership_title}}</div>

                                        @if ($errors->has('plan'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('plan') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="planfee" class="col-md-4 control-label">Amount To Pay</label>

                                    <div class="col-md-6">

                                        <div type="disabled" class="alert-success alert"  >{{ $membershipDetail->membership_fee}}</div>

                                        @if ($errors->has('plan'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('plan') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <input type="hidden" name="gen_unique_transaction_id" value="{{ $uniqueTransaction }}" >
                                <input type="hidden" name="membership_order_number" value="{{ $membershipOrderNumber }}" >
                                <input type="hidden" name="email" value="{{$thisUser->email}}"> {{-- required --}}
                                <input type="hidden" name="orderID" value="345">
                                <input type="hidden" name="amount" value="{{$membershipDetail->membership_fee*100}}"> {{-- required in kobo --}}
                                <input type="hidden" name="quantity" value="3">
                                <input type="hidden" name="reference" value="{{ $uniqueTransaction }}"> {{-- required --}}
                                <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                                {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        {{--<button type="submit" class="btn btn-primary btn-block">--}}
                                            {{--Click To Pay Now--}}
                                        {{--</button>--}}
                                        <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
                                            <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
