@extends('membership.sublayout')

@php($title = isset($title)? $title : "View All Membership")

@php($menu = isset($menu)? $menu : 'members.index')


@section('subcontent')
   <div class="panel panel-default">
                <div class="panel-heading">All Uplift Members</div>

                <div class="panel-body">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Full Name</th>
                                <th>Gender</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = 1;
                        @endphp
                        @foreach($members as $eachMembers)
                            @php($memberInfo = \App\Models\UserInfo::where('user_id', $eachMembers->id)->first())
                            <tr>
                                <td>{{ $count++ }}</td>
                                <td>{{$eachMembers->name}}</td>
                                <td>{{$memberInfo->gender == 'M'? 'Male':'Female'}}</td>
                                <td><a href="#" class="btn btn-success">AnyAction</a></td>
                                <td>{!!$eachMembers->status == 1? '<label class="label-success label">Active Member</label>' : '<label class="label-warning label">InActive Member</label>'!!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
    </div>
@endsection
