<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
                [
                    [
                        'name' => 'Abuango Sadiq',
                        'email' => 'h4xx22live@gmail.com',
                        'password' => bcrypt('secret'),
                        'level' => 1,
                        'status' => 1,
                        'remember_token' => NULL,
                        'created_at' => date('Y-m-d H:m:s'),
                        'updated_at' => date('Y-m-d H:m:s')
                    ],
                    [
                        'name' => 'Light Chinaka',
                        'password' => bcrypt('secret'),
                        'email'=> 'chinakalight@googlemail.com',
                        'level' => 1,
                        'status' => 1,
                        'remember_token' => NULL,
                        'created_at' => date('Y-m-d H:m:s'),
                        'updated_at' => date('Y-m-d H:m:s')
                    ]
                ]
            );
    }
}
