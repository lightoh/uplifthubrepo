<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcomeindex');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route to Manage Membership
Route::resource('membership', 'CreateMembershipController');

//Route to Manage Members
Route::resource('members', 'ManageMemberController');


//Route to Accomodate Selection Of Membership Plan
Route::get('register/{register}', '\App\Http\Controllers\Auth\RegisterController@proceedRegistration');

// Payment Confirmation
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');

// Route to The Payment Controller
Route::resource('payment', 'PaymentController', ['only'=>['show']]);
//Route::resource('payment', 'PaymentController', ['only'=>['show', 'redirectToGateway', 'handleGatewayCallback']]);

// Payment Form
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay'); // Laravel 5.1.17 and above

