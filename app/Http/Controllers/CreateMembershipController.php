<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Membership;

class CreateMembershipController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allMembership = Membership::all();
        return view('membership.index')->withTitle("All Membership")->withMemberships($allMembership);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // creates a symbolic file to the storage on the server
        if(!file_exists(public_path('storage'))) {
            \App::make('files')->link(storage_path('app/public'), public_path('storage'));
        }

        return view('membership.create')->withTitle("Create Membership");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validateMembership($request);
        
        if($request->hasFile('image')){
            $membershipImage = $request->file('image')->store('public/images');
        }

        if(!empty($membershipImage)){
            Membership::create([
                'membership_title' => $request->input('membership_title'),
                'membership_fee' => $request->input('membership_fee'),
                'description' => $request->input('membership_desc'),
                'image' => $membershipImage
            ]);
        }else{
            Membership::create([
                'membership_title' => $request->input('membership_title'),
                'membership_fee' => $request->input('membership_fee'),
                'description' => $request->input('membership_desc'),
                'image' => ""
            ]);
        }

        return redirect('membership')->with("success_message", "Membership Created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thisMembership = Membership::find($id);
        return view('membership.show')->withTitle("View  Membership")->withMembership($thisMembership);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thisMembership = Membership::find($id);
        return view('membership.edit')->withTitle("Edit/Update Membership")->withMembership($thisMembership);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validateMembership($request);


        if($request->hasFile('image')){
            $newMembershipImage = $request->file('image')->store('public/images');
        }

        $thisMembership = Membership::find($id);

        if($thisMembership->exists()){
            
            if(!empty($newMembershipImage)){

                $thisMembership->membership_title = $request->input('membership_title');
                $thisMembership->membership_fee = $request->input('membership_fee');
                $thisMembership->description = $request->input('membership_desc');

                $oldImage = $thisMembership->image;
                $thisMembership->image = $newMembershipImage; // store the new image

                if(\Storage::exists($oldImage)){
                    \Storage::delete($oldImage);
                }
                
                $thisMembership->save();
                
            }else{
                if($thisMembership->exists()){
                    $thisMembership->membership_title = $request->input('membership_title');
                    $thisMembership->membership_fee = $request->input('membership_fee');
                    $thisMembership->description = $request->input('membership_desc');
                    $thisMembership->save();
                }
            }
            
            return redirect('membership/'.$thisMembership->id)->with('success_message', 'Membership Successfully Updated');

        }else{
            return back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thisMembership = Membership::find($id);
        if($thisMembership->status == 1){
            $thisMembership->status = 0;
            $thisMembership->save();
            $result_message = "Successfully De-Activated";
        }else{
            $thisMembership->status = 1;
            $result_message = "Successfully Activated";
            $thisMembership->save();
        }

        return redirect('membership')->with('success_message', $result_message);
    }

    private function validateMembership(Request $request)
    {
        return $this->validate($request, [
            'membership_title' => 'required|string|min:5',
            'membership_fee' => array('required', 'regex:/^\d*(\.\d{2})?$/'),
            'membership_desc' => 'required|string|min:5',
        ]);
    }
}
