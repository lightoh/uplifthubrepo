<?php

namespace App\Http\Controllers\Auth;

use App\Models\UserInfo;
use App\Models\UserMembership;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Membership;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    public static  $TRANSACTION_TYPE = 1;
    public static  $MEMBER_ORDER_TYPE = 2;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $allMembership = Membership::where('status', 1)->get()->all();
        return view('auth.register')->withTitle("New Member Registration")->withMemberships($allMembership);
    }

    /**
     * Proceed After Membership Plan is Selected Registration
     *
     * @return \Illuminate\Http\Response
     */
    public function proceedRegistration(Request $request, $membershipPlanId)
    {
        $thisMembershipPlan = Membership::where('status', 1)
            ->where('id', $membershipPlanId)->get()->first();



        if(count($thisMembershipPlan) == 0){
            return redirect('register');
        }

        \Session::put('mPlan', $thisMembershipPlan->id); // store membershipPlan selected

        $allMembership = Membership::where('status', 1)->get()->all();
        return view('auth.register')->withTitle("New Member Registration")->withMemberships($allMembership)->withPlan($thisMembershipPlan->id)->withCurrentMemberPlan($thisMembershipPlan);
    }


     /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $this->validatorTwo($request->all())->validate();

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $othername = $request->input('othername');
        $phoneno = $request->input('phoneno');
        $gender = $request->input('gender');
        $email = $request->input('email');
        $password = $request->input('password');
        $membershipPlanId = $request->input('plan');

        // Check If this plan exists and is valid thus
        $checkMembership = Membership::find($membershipPlanId);

        if(!$checkMembership->exists()){
            return redirect('register'); // Return to the register
        }

        $name = "$firstname $lastname";

        $data = ['name'=>$name, 'email'=>$email, 'password'=>$password, 'level'=>2, 'status'=>2];


        event(new Registered($user = $this->create($data)));


        //Create/Store the UserInfo using the user just created
        UserInfo::create([
            'user_id'=>$user->id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'othername' => $othername,
            'gender' => $gender,
            'phoneno' => $phoneno,
        ]);

        $membershipOrderNumber = RegisterController::generateUniqueString(RegisterController::$MEMBER_ORDER_TYPE);

        //Record The Membership Plan Selected
        UserMembership::create([
            'user_id' => $user->id,
            'membership_id' => $membershipPlanId,
            'membership_order_number' => $membershipOrderNumber,
            'payment_status' => 0, // 0 - means not yet paid, 1 - means paid
        ]);

        return redirect('payment/'.$membershipOrderNumber);

        // $this->guard()->login($user);  // Automatic Login Is Not Required
//        return $this->registered($request, $user)
//                        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorTwo(array $data)
    {
        return Validator::make($data, [
            'plan'=> 'required|integer|min:1',
            'firstname' => 'required|string|max:150',
            'lastname' => 'required|string|max:150',
            'othername'=> 'string',
            'phoneno' => 'required|string|max:15',
            'gender' => 'required|string|max:1',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'level' => $data['level'],
            'status' => $data['status'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }


    /**
     * generateUniqueString Function generates unique string depending on the parameters
     * passed to it
     * @param null | $this->TRANSACTION_TYPE | $this->MEMBER_ORDER_TYPE $type
     *
     * @return bool|string
     */
    public static function generateUniqueString($type = null){
        $continue_check = TRUE;
        while($continue_check)
        {
            $getU_string = str_shuffle(substr(rand(10000000000, rand(999999999998, 8992929999999)), 0, 10));
            $transact_str = str_shuffle(str_shuffle(sha1(sha1($getU_string))));
            $tractionstr2 = sha1(md5(str_shuffle('sal445&8*'.time()),time().rand(109878, 9999999999))).sha1(md5(str_shuffle('sal4%%%%45'.time()),time().rand(109878, 9999999999)));

            $transact_str .= $tractionstr2;

            $count = 0;

            if($type == RegisterController::$TRANSACTION_TYPE ){
                $transact_str = substr(str_shuffle($transact_str), rand(0, 10), 50);
                $count = \DB::table('payment_transactions')->where('transaction_number', $transact_str)->count();
            }else if($type == RegisterController::$MEMBER_ORDER_TYPE){

                $transact_str = substr(str_shuffle("$transact_str$getU_string"), rand(0, 5), 100);
                $count = \DB::table('user_memberships')->where('membership_order_number', $transact_str)->count();
            }

            if($count == 0){
                return $transact_str;
            }

        }
    }
}
