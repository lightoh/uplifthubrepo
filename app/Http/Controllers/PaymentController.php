<?php

namespace App\Http\Controllers;

use App\Models\Membership;
use App\Models\UserMembership;
use Illuminate\Http\Request;
use App\Models\PaymentTransactions;
use \App\Http\Controllers\Auth\RegisterController;
use Paystack;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except'=>['show', 'redirectToGateway', 'handleGatewayCallback']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $payment
     * @return \Illuminate\Http\Response
     */
    public function show($membershipOrderNumber)
    {
        $userMembership = UserMembership::where('membership_order_number', $membershipOrderNumber)
            ->where('payment_status', 0)->get()->first();

        if(is_null($userMembership)){
            return redirect('home')->with('error_message', 'Wrong/Invalid Request');
        }

        $membershipDetail = Membership::find($userMembership->membership_id);

//        dd($membershipDetail);

        $transaction_number = RegisterController::generateUniqueString(RegisterController::$TRANSACTION_TYPE);
//        $transaction_number = Paystack::genTranxRef();

        //
        PaymentTransactions::create([
            'membership_order_number' => $membershipOrderNumber,
            'transaction_number' => $transaction_number,
            'amount' => $membershipDetail->membership_fee,
            'payment_status' => 0,
            'vendor_transaction_no' => "",
            'merchant_id' => ""
        ]);

        return view('payment.index')->with('membershipDetail', $membershipDetail)
            ->with('userMembership', $userMembership)
            ->with('thisUser', \App\User::find($userMembership->user_id))
            ->with('uniqueTransaction', $transaction_number)
            ->with('membershipOrderNumber', $membershipOrderNumber);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
//        dd("WORKING");
        $paymentDetails = Paystack::getPaymentData();

//        dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want

        // Get the Reference and update the database table

        if($paymentDetails['status'] == true && $paymentDetails['message'] == 'Verification successful' && $paymentDetails['data']['status'] == 'success'){

            $transaction_reference = $paymentDetails['data']['reference'];
            $authorization_code = $paymentDetails['data']['authorization']['authorization_code'];
            // get the previous transaction
            $payTrans = PaymentTransactions::where('transaction_number', $transaction_reference)
                ->where('payment_status', 0)->get()->first();
//            dump($transaction_reference);
            if(is_null($payTrans)){
                return redirect('home')->with('error_message', 'Something Not Alright!' );
            }

            $payTrans->payment_status = 1;
            $payTrans->vendor_transaction_no = $_GET['trxref'];
            $payTrans->updated_at = \Carbon\Carbon::now();
            $payTrans->save();

            $thisUserMem = UserMembership::where('membership_order_number', $payTrans->membership_order_number)->get()->first();

            $thisUserMem->payment_status = 1;
            $thisUserMem->save();

            $user = \App\User::find($thisUserMem->user_id);

            $user->status = 1;
            $user->save();

            Auth::login($user, true);


            return redirect('home')->with('success_message', 'Payment Successfully Completed. You Can Now Login Please');

        }
        return redirect('home')->with('error_message', 'Payment Wasn\'t Comleted Successfully' );

    }
}
