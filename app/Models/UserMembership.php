<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMembership extends Model
{
    //fillable fields
    protected $fillable = [
        'user_id', 'membership_id', 'membership_order_number', 'payment_status'
    ];

}
