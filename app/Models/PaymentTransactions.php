<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTransactions extends Model
{
    /**
     * @var array
     * Fillable Attributes
     */
    protected $fillable = [
        'membership_order_number', 'transaction_number', 'amount', 'payment_status',
        'vendor_transaction_no', 'merchant_id'
    ];

}
